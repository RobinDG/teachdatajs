Find more information in the [Wiki](https://bitbucket.org/RobinDG/teachdatajs/wiki/Home).

# Graphically Correct

The "Graphically correct!" app aims at being a simple and elegant tool for visualizing and analyzing student results data. It has been built with the following principles in mind:

- Ease of use: drag and drop your .csv or .xlsx data file in the app, and that's it!
- Local: your data stays on your computer
- Open-source: the code is on [BitBucket](https://bitbucket.org/RobinDG/teachdatajs/src/master/)
- Free

The app was created by [Robin De Gernier](https://robindg.pythonanywhere.com/).

It is built on top of the following tools:

- [Electron](https://www.electronjs.org/)
- [Bootstrap](https://getbootstrap.com/)
- [Grid.js](https://gridjs.io/)
- [Plotly Javascript](https://plotly.com/javascript/)
- [Danfo.js](https://danfo.jsdata.org/)
- [SheetJS](https://github.com/sheetjs/sheetjs)
