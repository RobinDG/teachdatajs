const path = require('path')
const fs = require ('fs')
const os = require ('os')
const { Grid } = require('gridjs')
const shell = require('electron').shell
const remote = require('electron').remote
const XLSX = require('xlsx')
require('bootstrap')

// Default values
var GRIDPAGELIMIT = 50 // number of lines in data grid visualization
var SUMMARYINPERCENT = true // display summary box plots in percent
var HISTOBINSIZE = null // size of bins in histograms
var ACTIVEWARNINGS = true // toggle warnings for missing data
// Global variables
var data_file
var df_noNaN // result of load_process_data()
var df_converted // result of clean_data(df_noNaN)
var appVersion = remote.app.getVersion(); // get version number (defined in package.json)

// Display version number in "About" modal and at bottom of pages
document.getElementById("aboutVersionNumber").innerText = `Version ${appVersion}`;
document.getElementById("footnotesVersionNumber").innerText = `Grapically Correct v${appVersion}`;

////
// Check for available updates
////
const updater = {
    releases: null,
    findLatestRelease: async function () {
        const parser = new DOMParser();
        const rep = await fetch('https://bitbucket.org/RobinDG/teachdatajs/downloads/', {cache: "no-store", headers: {'Cache-Control': 'no-cache'}})
        const txt = await rep.text()
        const html = await parser.parseFromString(txt, 'text/html');
        var newestVersion = "0.0.0"; // Initialize
        html.querySelectorAll('#uploaded-files tbody tr td.name').forEach((td) => {
            try {
                let vString = td.innerText.match(/[0-9]+\.[0-9]+\.[0-9]+/g)[0]
                newestVersion = this.maxVersion(newestVersion, vString)
            } catch (error) {
                if (error instanceof TypeError) {
                } else {
                    console.log("Error met while checking for updates.")
                    console.log(error)
                }
            }
        });
        return newestVersion
    },

    checkVersion: async function (userDemand=false) {
        const updateAvailableInfo = document.getElementById('updateAvailableInfo');
        const updateReleaseNotes = document.getElementById('updateReleaseNotes');
        const upToDateModalBtn = document.getElementById('upToDateBtn');
        const updateModalBtn = document.getElementById('updateAvailableBtn');

        const newestVersion = await this.findLatestRelease()

        if (appVersion == this.maxVersion(appVersion, newestVersion)) {
            upToDateInfo.innerText = `You are currently running the latest version of Graphically Correct (v${appVersion}). All is well!`;
            if (userDemand) {
                // Only display the "up to date" modal if the user explicitly checked for updates
                upToDateBtn.click();
            }
        } else {
            this.releases = await this.fetchReleaseNotes()
            updateAvailableInfo.innerText = `You are currently running version ${appVersion} of Graphically Correct. Version ${newestVersion} is now available.`;
            if (this.releases[newestVersion]) {
                if (updateReleaseNotes.hasChildNodes()) {
                    updateReleaseNotes.childNodes.forEach((child) => {child.remove()})
                    updateReleaseNotes.innerHTML = ""
                }
                this.releases[newestVersion].changes.forEach((item) => {
                    const li = document.createElement("LI")
                    li.innerText = `[${item.type}] ${item.description}`
                    updateReleaseNotes.appendChild(li)
                });
            }
            updateModalBtn.click();
        }
    },

    fetchReleaseNotes: async () => {
        const rep = await fetch('https://bitbucket.org/RobinDG/teachdatajs/downloads/releases.json', {cache: "no-store"})
        if (rep.status == 200) {
            const myJson = rep.json()
            return myJson
        } else {
            return JSON.stringify({})
        }
    },

    maxVersion: function (v1String, v2String) {
        let vMax
        let v1 = v1String.split('.')
        let v2 = v2String.split('.')

        if (v1[0] > v2[0]) {
            vMax = v1String
        } else if (v1[0] == v2[0]) {
            // Check second level of version number
            if (v1[1] > v2[1]) {
                vMax = v1String
            } else if (v1[1] == v2[1]) {
                // Check third (last) level of version number
                if (v1[2] > v2[2]) {
                    vMax = v1String
                } else {
                    vMax = v2String
                }
            } else {
                vMax = v2String
            }
        } else {
            vMax = v2String
        }

        return vMax
    }
}


// Check for update at each launch of the app
updater.checkVersion();

// Add click logic to the "check-update" button
document.getElementById("btn-check-update").addEventListener('click', () => {
    // Check for update on user demand
    updater.checkVersion(userDemand=true)
})


////
// File importation
////
async function handleFile() {
    if ((data_file.substr(-4) === ".csv") || (data_file.substr(-5) === ".xlsx")) {
        ACTIVEWARNINGS = true
        df_noNaN = await load_process_data()
        df_converted = await clean_data(df_noNaN)
        document.getElementById("btn-data").click()
        document.getElementById("main-pannel").style.background = "white"
        document.getElementById("welcome-screen-text").style.display = "none"
        document.getElementById("sidebarMenuDiv").style.display = "block"
    } else {
        alert("This is not a .csv or .xlsx file. Please use a .csv or .xlsx file")
        document.getElementById("main-pannel").style.background = "white"
        document.body.style.backgroundColor = "white"
        document.getElementById("welcome-screen-text").style.display = "block"
        document.getElementById("sidebarMenuDiv").style.display = "none"
        document.getElementById("wrap-data").style.display = "none"
        document.getElementById("wrap-graphs").style.display = "none"
        document.getElementById("wrap-stats").style.display = "none"
    }
}

// Drag and drop file
document.addEventListener('drop', async (e) => {
    e.preventDefault();
    e.stopPropagation();

    for (const f of e.dataTransfer.files) {
        console.log('File(s) you dragged here: ', f.path)
        data_file = await f.path
    }

    handleFile()
    document.getElementById("main-pannel").style.background = "white"
    document.body.style.background = "white"
});
document.addEventListener('dragover', (e) => {
    e.preventDefault();
    e.stopPropagation();
    document.getElementById("main-pannel").style.background = "#a3c2c2"
    document.body.style.backgroundColor = "#a3c2c2"
});
document.addEventListener('dragleave', (e) => {
    e.preventDefault();
    e.stopPropagation();
    document.getElementById("main-pannel").style.background = "white"
    document.body.style.background = "white"
});

// Import file with file selector
document.getElementById("btn-import-data").addEventListener("click", () => {
    document.getElementById("main-pannel").style.background = "#a3c2c2"
    document.body.style.backgroundColor = "#a3c2c2"
    document.getElementById("importButton").value = "" // reset file input (without this, no change is detected when loading a file with the same name as the one previously loaded)
    document.getElementById("importButton").click()
})
document.getElementById("importButton").addEventListener("change", ({ target: { files } }) => {
    data_file = files[0].path
    handleFile()
    document.getElementById("main-pannel").style.background = "white"
    document.body.style.background = "white"
})


async function loadXLSX(xlsxFile) {
    const workbook = await XLSX.readFile(xlsxFile)
    const sheetNamesList = workbook.SheetNames
    // Export to temporary json and replace empty cells by '_NA_'
    const jsonData = await XLSX.utils.sheet_to_json(workbook.Sheets[sheetNamesList[0]], {defval: '_NA_'})
    const worksheet = workbook.Sheets[sheetNamesList[0]]
    // Create dataframe from json
    const df = new dfd.DataFrame(jsonData)

    // Check for false "empty" XLSX cells
    // We cannot use df.replace({ "replace": '', "with": '_NA_' }) because it replaces "0" as well)
    df.data = df.data.map((row) => {return row.map((val) => {return (val === "") ? '_NA_' : val})})

    // Alert user if missing values are found
    alertForMissingValues(df)
    // Disable alerts for the rest of the session (until new file is imported)
    ACTIVEWARNINGS = false

    return df
}


async function alertForMissingValues(df) {
    const missDataDiv = document.getElementById("container-missing-data")
    const myModal = document.getElementById('missingDataModal')
    const myModalBtn = document.getElementById('missingDataBtn')
    const indexMissingVals = df.apply({callable: (val) => {return val === '_NA_'} })
                    .sum({axis: 0}).data.map((row, rowIndex) => {return [rowIndex, row]})
                    .filter(([rowIndex, row]) => {return row > 0})
                    .map(([rowIndex, row]) => {return rowIndex})

    // If missing values are detected and warnings are activated,
    // show the missing values in a grid embedded in a modal
    if ((indexMissingVals.length > 0) && ACTIVEWARNINGS) {
        const df_missing = df.loc({rows: indexMissingVals})
        df_missing.addColumn({ "column": "Row #", "value": indexMissingVals.map((val) => {return val + 2}) })

        // Prepare a fresh div to contain the data grid
        if (missDataDiv.hasChildNodes()) {
            oldDiv = missDataDiv.childNodes[0]
            oldDiv.remove()
        }
        newDiv = document.createElement("DIV")
        newDiv.id = "missingDataGrid"
        missDataDiv.append(newDiv)

        const grid = new Grid({
            columns: df_missing.columns,
            data: df_missing.data,
            pagination: false,
            fixedHeader: true,
            height: 400,
            search: false,
            sort: true
        }).render(newDiv)

        myModalBtn.click()
    }
    return
}


async function load_process_data() {
    var df = await loadXLSX(data_file)

    // Remove missing values (TODO: add warning to the user)
    let df_dropna = df.dropna({axis: 0})

    return df_dropna
}


async function clean_data(df) {
    // Identify columns containing results (the name of those columns should contain the number of points in the format "/20")
    let cols = df.columns.filter(col => col.match(/\/\d/))
    // Display warning if no columns match with the naming convention
    if (cols.length === 0) {
        document.getElementById("noColsBtn").click()
    }

    // Convert results columns to float and get rid of the text values (such as "A", "ABS", "NDP"...)
    var df_converted = df.copy()

    cols.forEach((col) => {
        df_converted = df_converted.astype({column: col, dtype:"float32"})
    })

    return df_converted
}


function normalizeData(data, max) {
    return data.map((value) => {
        if (typeof value == "number") {
            return 100 * value / max
        } else {
            return value
        }
    })
}

async function createSummaryBox(df, div, inPercent = SUMMARYINPERCENT) {
    var data = []
    var q
    var maxQ = 0
    var dfMin = 0
    await df.columns.forEach((col) => {
        q = parseFloat(col.match(/\/(\d+.\d+|\d+,\d+|\d+)/)[1].replace(",", "."))
        maxQ = Math.max(maxQ, inPercent ? 100 * df[col].max() / q : q)
        dfMin = Math.min(dfMin, inPercent ? 100 * df[col].min() / q : 0)
        const colData = inPercent ? normalizeData(df[col].data, q) : df[col].data
        data.push({y: colData, type: 'box', name: col})
    })

    const ytitle = inPercent ? "Score in %" : "Score"
    const ymax = inPercent ? Math.max(110, maxQ) : null
    var layout = {width: document.getElementById("wrap-graphs").offsetWidth, yaxis: {title: ytitle, range: [dfMin, ymax]}}
    var config = {
        responsive: true,
        editable: true,
        showLink: true,
        plotlyServerURL: "https://chart-studio.plotly.com",
        linkText: 'Edit chart online in Plotly Chart Studio'
    }

    Plotly.newPlot(div.id, data, layout, config)
}


async function generateGraphs(inPercent = SUMMARYINPERCENT) {
    const df = df_converted
    // Identify columns containing results (the name of those columns should contain the number of points in the format "/20")
    let cols = df.columns.filter(col => col.match(/\/\d/))


    var graphs_summary = document.getElementById("graphs-summary-contents")
    if (graphs_summary.hasChildNodes()) {
        graphs_summary.childNodes.forEach((child) => {child.remove()})
        graphs_summary.innerHTML = ""
    }

    ////// Summary tab //////
    var figDiv = document.createElement("DIV")
    figDiv.id = "divAllFigs"
    document.getElementById("graphs-summary-contents").appendChild(figDiv)

    var layout = {width: document.getElementById("wrap-graphs").offsetWidth, responsive: true}

    createSummaryBox(df.loc({columns: cols}), figDiv, inPercent)
}

document.getElementById("graphs-summary-tab").addEventListener("click", () => {generateGraphs(SUMMARYINPERCENT)})

// Program the buttons for setting the scale of the summary graph
const summaryButtons = document.querySelectorAll(".summary-btn")
summaryButtons.forEach((btn, btnIndex, btnArray) => {
    const sumOptions = ["percent", "values"]
    sumOptions.forEach((value) => {
        if (btn.classList.contains(`summary-btn-${value}`)) {
            btn.addEventListener('click', () => {
                SUMMARYINPERCENT = (value === "percent")
                generateGraphs(SUMMARYINPERCENT)
                btnArray.forEach((clickedBtn) => {
                    clickedBtn.classList.remove("active")
                })
                btn.classList.add("active")
            })
        }
    })
})


async function chooseHoverTemplate(text, name) {
    if ((text.constructor === Array) && (text.length > 0)) {
        var hovertemplate = `<i>${name}</i>: %{y:.2f}` +
                    '<br><br>%{text}' +
                    '<extra></extra>'
    } else {
        var hovertemplate = `<i>${name}</i>: %{y:.2f}` +
                        '<extra></extra>'
    }

    return hovertemplate
}

async function createViolinPlot(y, div, name, text) {
    var q = parseFloat(name.match(/\/(\d+.\d+|\d+,\d+|\d+)/)[1].replace(",", "."))
    var hovertemplate = await chooseHoverTemplate(text, name)

    var data = [
        {
            y: y,
            points: 'all',
            jitter: 0.3,
            box: {
                visible: true
            },
            meanline: {
                visible: true
            },
            type: 'violin',
            name: name,
            hoveron: "violins+points",
            hovertemplate: hovertemplate,
            text: text,
        }
    ];

    var config = {
        responsive: true,
        editable: true,
        showLink: true,
        plotlyServerURL: "https://chart-studio.plotly.com",
        linkText: 'Edit chart online in Plotly Chart Studio'
    }
    var layout = {title: name, width: document.getElementById("wrap-graphs").offsetWidth, yaxis: {title: "Points /" + q}}

    Plotly.newPlot(div.id, data, layout, config);
}


async function createBoxPlot(y, div, name, text) {
    var q = parseFloat(name.match(/\/(\d+.\d+|\d+,\d+|\d+)/)[1].replace(",", "."))
    var hovertemplate = await chooseHoverTemplate(text, name)
    var data = [
        {
            y: y,
            boxpoints: 'all',
            jitter: 0.3,
            pointpos: -1.5,
            meanline: {
                visible: true
            },
            type: 'box',
            name: name,
            hovertemplate: hovertemplate,
            text: text,
        }
    ];

    var config = {
        responsive: true,
        editable: true,
        showLink: true,
        plotlyServerURL: "https://chart-studio.plotly.com",
        linkText: 'Edit chart online in Plotly Chart Studio'
    }

    var layout = {title: "", width: document.getElementById("wrap-graphs").offsetWidth, yaxis: {title: "Points /" + q}}

    Plotly.newPlot(div.id, data, layout, config);
}


function addColNames(value, i, array) {
    return "<i>" + this[i] + "</i>: " + value
}


async function generateHoverText(df) {
    var anticols = df.columns.filter(col => !col.match(/\/\d/))
    if (anticols.length != 0) {
        var text = df.loc({columns:anticols}).data.map((value) => {return value.map(addColNames, anticols).join('<br>')})
    } else {
        var text = ""
    }
    return text
}


function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}


async function createPieChart(values, div, name) {
    var countNum = values.filter((v) => {return (((typeof parseFloat(v)) === 'number') && (!isNaN(parseFloat(v))))}).length
    var countNaN = values.filter((v) => {return (((typeof v) === 'number') && isNaN(v))}).length
    var uniqueStrings = values.filter((v) => {return (((typeof v) === 'string') && (isNaN(v)))}).filter(onlyUnique)

    var countUniqueStrings = []
    uniqueStrings.forEach((item) => {
        countUniqueStrings.push(values.filter((v) => {return (v === item)}).length)
    })


    var data = [{
        values: [countNum].concat(countUniqueStrings.concat([countNaN])),
        labels: ["Participants"].concat(uniqueStrings.concat(["NaN"])),
        type: 'pie',
        sort: false,
    }];

    var layout = {
        title: name,
        width: document.getElementById("wrap-graphs").offsetWidth
    };

    var config = {
        responsive: true,
        editable: true,
        showLink: true,
        plotlyServerURL: "https://chart-studio.plotly.com",
        linkText: 'Edit chart online in Plotly Chart Studio'
    }
    Plotly.newPlot(div.id, data, layout, config);
}



async function generateDetailsGraphs() {
    const df = df_converted
    // Identify columns containing results (the name of those columns should contain the number of points in the format "/20")
    let cols = df.columns.filter(col => col.match(/\/\d/))
    var text = await generateHoverText(df)

    // Delete existing graphs and buttons
    var graphs_details_ul = document.getElementById("graphs-details-ul")
    if (graphs_details_ul.hasChildNodes()) {
        graphs_details_ul.childNodes.forEach((child) => {child.remove()})
        graphs_details_ul.innerHTML = ""
    }
    var graphs_contents = document.getElementById("graphs-details-contents")
    if (graphs_contents.hasChildNodes()) {
        graphs_contents.childNodes.forEach((child) => {child.remove()})
        graphs_contents.innerHTML = ""
    }

    cols.forEach(async (col, index, array) => {
        layout = {title: col}

        ////// Details tab //////
        // Create "details" tab buttons and container division
        details = await createNavPillElements(col, "col", index)
        var detailsLi = details[0]
        var detailsDiv = details[1]

        // Add divisions for the detail graphs
        var detailsViolin = document.createElement("DIV")
        detailsViolin.id = "col-" + index + "-violin"
        detailsDiv.appendChild(detailsViolin)

        var detailsBox = document.createElement("DIV")
        detailsBox.id = "col-" + index + "-box"
        detailsDiv.appendChild(detailsBox)

        var detailsPie = document.createElement("DIV")
        detailsPie.id = "col-" + index + "-pie"
        detailsDiv.appendChild(detailsPie)

        document.getElementById("graphs-details-ul").appendChild(detailsLi)
        document.getElementById("graphs-details-contents").appendChild(detailsDiv)


        // Draw detail graphs
        detailsLi.addEventListener("click", () => {
            createViolinPlot(df[col].data, detailsViolin, col, text)
            createBoxPlot(df[col].data, detailsBox, col, text)
            createPieChart(df_noNaN[col].data, detailsPie, col)
        })

    })
}

document.getElementById("graphs-details-tab").addEventListener("click", generateDetailsGraphs)

// Lazy loading options
let options = {
  rootMargin: '300px 0px 300px 0px',
  threshold: 0.
}

function lazyGenerateVsGraphs(entries, observer) {
    entries.forEach((entry) => {
        let graphDiv = entry.target
        let graph = vsGraphList.filter((gr) => {return gr.div.id == graphDiv.id})[0]

        if (!entry.isIntersecting) {
            return
        } else {
            // Ensure correct scaling of graphs
            graphDiv.style.minHeight = "auto"
            graphDiv.style.height = "auto"
            graph.generate()
            observer.unobserve(graphDiv)
        }
    });
}

let vsGraphObserver = new IntersectionObserver(lazyGenerateVsGraphs, options)

class VsGraph {
    constructor(id, hovertext, xlabel, ylabel) {
        this.id = id
        this.hovertext = hovertext
        this.xlabel = xlabel
        this.ylabel = ylabel
    }

    get div () {
        return document.getElementById(this.id)
    }

    generate () {
        createVsGraph(df_converted[this.xlabel].data, df_converted[this.ylabel].data, this.div, this.hovertext, this.xlabel, this.ylabel)
    }
}

async function createVsGraph(x, y, div, text, xlabel, ylabel) {
    var hovertemplate = await chooseHoverTemplate(text, ylabel)
    hovertemplate = `<i>${xlabel}</i>: %{x:.2f}<br>` + hovertemplate
    var data = [{
        x: x,
        y: y,
        hovertemplate: hovertemplate,
        text: text,
        mode: 'markers',
        type: 'scatter'
    }]

    var layout = {title: "", xaxis: {title: xlabel}, yaxis: {title: ylabel}, hovermode: "closest", width: document.getElementById("wrap-graphs").offsetWidth}
    var config = {
        responsive: true,
        editable: true,
        showLink: true,
        plotlyServerURL: "https://chart-studio.plotly.com",
        linkText: 'Edit chart online in Plotly Chart Studio'
    }

    Plotly.newPlot(div.id, data, layout, config)
}


var vsGraphList
async function generateVersusGraphs() {
    vsGraphList = []
    let df = df_converted
    // Identify columns containing results (the name of those columns should contain the number of points in the format "/20")
    let cols = df.columns.filter(col => col.match(/\/\d/))
    var text = await generateHoverText(df)

    // Delete existing graphs and buttons
    var graphs_vsgraphs_ul = document.getElementById("graphs-vsgraphs-ul")
    if (graphs_vsgraphs_ul.hasChildNodes()) {
        graphs_vsgraphs_ul.childNodes.forEach((child) => {child.remove()})
        graphs_vsgraphs_ul.innerHTML = ""
    }
    var graphs_vs_contents = document.getElementById("graphs-vsgraphs-contents")
    if (graphs_vs_contents.hasChildNodes()) {
        graphs_vs_contents.childNodes.forEach((child) => {child.remove()})
        graphs_vs_contents.innerHTML = ""
    }


    cols.forEach(async (col, index, array) => {
        ////// Versus graphs tab //////
        // Create "versus" tab buttons and container division
        var [vsGraphsLi, vsGraphsDiv] = await createNavPillElements(col, "vs", index)

        document.getElementById("graphs-vsgraphs-ul").appendChild(vsGraphsLi)
        document.getElementById("graphs-vsgraphs-contents").appendChild(vsGraphsDiv)

        vsGraphsLi.addEventListener("click", () => {

            vsGraphList.forEach((vsgraph) => {
                const graphDiv = vsgraph.div
                vsGraphObserver.unobserve(graphDiv)
                graphDiv.remove()
            })
            vsGraphList = []

            // Add vs graphs
            cols.forEach(async (subcol, subindex, subcolarray) => {
                if (subcol != col) {
                    var graphSubDivID = "col-" + index + "-vs-" + subindex
                    if (!document.getElementById(graphSubDivID)) {
                        var vsGraphSubDiv = document.createElement("DIV")
                        vsGraphSubDiv.id = graphSubDivID
                        vsGraphSubDiv.style.minHeight = "300px"
                        vsGraphsDiv.appendChild(vsGraphSubDiv)
                    } else {
                        vsGraphSubDiv = document.getElementById(graphSubDivID)
                    }

                    const vsgraph = new VsGraph(vsGraphSubDiv.id, text, col, subcol)
                    vsGraphList.push(vsgraph)
                    vsGraphObserver.observe(vsGraphSubDiv)
                }
            })
        })
    })
}

document.getElementById("graphs-vsgraphs-tab").addEventListener("click", generateVersusGraphs)

function lazyGenerateHistoGraphs(entries, observer) {
    entries.forEach((entry) => {
        let graphDiv = entry.target
        let graph = histoGraphList.filter((gr) => {return gr.div.id == graphDiv.id})[0]

        if (!entry.isIntersecting) {
            return
        } else {
            // Ensure correct scaling of graphs
            graphDiv.style.minHeight = "auto"
            graphDiv.style.height = "auto"
            graph.generate()
            observer.unobserve(graphDiv)
        }
    });
}

let histoGraphObserver = new IntersectionObserver(lazyGenerateHistoGraphs, options)

class HistoGraph {
    constructor(id, name) {
        this.id = id
        this.name = name
    }

    get div () {
        return document.getElementById(this.id)
    }

    generate () {
        createHisto(df_converted[this.name].data, this.div, this.name, HISTOBINSIZE)
    }
}


async function createHisto(x, div, name, binSize=null) {
    var q = parseFloat(name.match(/\/(\d+.\d+|\d+,\d+|\d+)/)[1].replace(",", "."))
    // Configure bin size based on the value passed in 'binSize' (string or number)
    var binSizeValue = ((binSize === "auto") | (binSize === null)) ? null : ((binSize === "10thmax") ? q/10 : ((binSize === "20thmax") ? q/20 : binSize) )
    var minValue = x.reduce((accumulator, val) => {return !isNaN(val) ? Math.min(val, accumulator) : accumulator}, 0)
    var maxValue = x.reduce((accumulator, val) => {return !isNaN(val) ? Math.max(val, accumulator) : accumulator}, q)
    var data = [{
        x: x,
        type: 'histogram',
        xbins: {
            start: minValue,
            size: binSizeValue,
            end: maxValue
        },
        marker: {
            line: {
                color:  "rgba(255, 255, 255, 1)",
                width: 2
            }
        },
    }]

    var config = {
        responsive: true,
        editable: true,
        showLink: true,
        plotlyServerURL: "https://chart-studio.plotly.com",
        linkText: 'Edit chart online in Plotly Chart Studio'
    }
    var layout = {title: name, width: document.getElementById("wrap-graphs").offsetWidth, yaxis: {title: "# Students"}, xaxis: {title: "Points /" + q, range:[minValue, maxValue], rangemode: "tozero"}}

    Plotly.newPlot(div.id, data, layout, config)
}

let histoGraphList = []
async function generateHistoGraphs(binSize=null) {
    const df = df_converted
    // Identify columns containing results (the name of those columns should contain the number of points in the format "/20")
    let cols = df.columns.filter(col => col.match(/\/\d/))

    histoGraphList.forEach((graph) => {
        const graphDiv = graph.div
        histoGraphObserver.unobserve(graphDiv)
        graphDiv.remove()
    });
    histoGraphList = []

    cols.forEach(async (col, index, array) => {
        ////// Histograms tab //////
        var figDiv = document.createElement("DIV")
        figDiv.id = "divFig" + index
        figDiv.style.minHeight = "300px"
        document.getElementById("histograms").appendChild(figDiv)
        let histoGraph = new HistoGraph(figDiv.id, col)
        histoGraphObserver.observe(figDiv)
        histoGraphList.push(histoGraph)
    })
}

document.getElementById("graphs-histograms-tab").addEventListener("click", () => {generateHistoGraphs(HISTOBINSIZE)})

// Program the buttons for setting the scale of the summary graph
const histoBinsButtons = document.querySelectorAll(".histo-bin-size-btn")
histoBinsButtons.forEach((btn, btnIndex, btnArray) => {
    const binSizes = ["auto", 0.2, 0.5, 1, "10thmax", "20thmax"]
    binSizes.forEach((value) => {
        if (btn.classList.contains(`histo-bin-size-btn-${value}`)) {
            btn.addEventListener('click', () => {
                generateHistoGraphs(value)
                HISTOBINSIZE = value
                btnArray.forEach((clickedBtn) => {
                    clickedBtn.classList.remove("active")
                })
                btn.classList.add("active")
            })
        }
    })
})


async function createNavPillElements(aInnerText, idPrefix, index) {

        // Create graphs details nav button for each result column
        var detailsLi = document.createElement("LI")
        detailsLi.classList.add("nav-item")
        detailsLi.setAttribute("role", "presentation")
        var detailsA = document.createElement("A")
        detailsA.classList.add("nav-link")
        detailsA.classList.add("graphs-nav")
        detailsA.classList.add("btn")
        detailsA.classList.add("btn-outline-secondary")
        detailsA.classList.add("btn-sm")
        detailsA.classList.add("m-1")
        detailsA.id = idPrefix + "-tab-" + index
        detailsA.setAttribute("data-toggle", "pill")
        detailsA.setAttribute("role", "tab")
        detailsA.setAttribute("aria-controls", "col-tab-" + index)
        detailsA.setAttribute("href", "#" + idPrefix + "-" + index)
        detailsA.setAttribute("aria-selected", "false")
        detailsA.innerHTML = aInnerText
        detailsLi.appendChild(detailsA)

        // Create divs that will contain detailed graphs for each result column
        var detailsDiv = document.createElement("DIV")
        detailsDiv.classList.add("tab-pane")
        detailsDiv.classList.add("fade")
        detailsDiv.setAttribute("role", "tabpanel")
        detailsDiv.setAttribute("aria-labelledby", idPrefix + "-tab-" + index)
        detailsDiv.id = idPrefix + "-" + index

        return [detailsLi, detailsDiv]
}


async function generate_stats() {
    const df_new = df_converted
    // Identify columns containing results (the name of those columns should contain the number of points in the format "/20")
    let cols = df_new.columns.filter(col => col.match(/\/\d/))

    const l_descr = []
    const d_stats = {n_students: [], p_success_reg: [], p_success_participants: []}

    cols.forEach((col) => {
        let col_descr = df_new.loc({columns: [col]}).dropna().describe()
        l_descr.push(col_descr)
        let q = parseFloat(col.match(/\/(\d+.\d+|\d+,\d+|\d+)/)[1].replace(",", "."))
        let num_success = df_new.loc({columns: [col]}).apply({callable: (v) => {if (v >= q/2) {return 1} else {return 0}}}).sum().values[0]
        let p_success_reg = 100 * num_success / df_new.data.length
        let p_success_participants = 100 * num_success / col_descr.loc({rows: ["count"]}).values[0][0]
        d_stats.n_students.push(df_new.data.length)
        d_stats.p_success_reg.push(parseFloat(p_success_reg.toFixed(2)))
        d_stats.p_success_participants.push(parseFloat(p_success_participants.toFixed(2)))
    })

    let df_descr = dfd.concat({ df_list: l_descr, axis: 1 }).round(2)

    const stat_descr = ["# participants", "mean", "std", "min", "median", "max", "variance", "# students", "% success (all students)", "% success (participants)"]
    const stat_descr_order = ["# students", "# participants", "% success (all students)", "% success (participants)", "mean", "median", "max", "min", "std", "variance"]
    let df_descr_full = new dfd.DataFrame(df_descr.data.concat([d_stats.n_students, d_stats.p_success_reg, d_stats.p_success_participants]), {columns: df_descr.columns, index:stat_descr})

    // Add column containing the names of each statistical measure for easier identification
    await df_descr_full.addColumn({"column":"Measure", "value":df_descr_full.index_arr})
    // Reorder the columns so that the first column is the one with the stats descriptions
    let cols_final = ["Measure"].concat(cols)
    let df_final = df_descr_full.loc({"columns": cols_final}).sortIndex()

    // Prepare a fresh div to contain the data grid
    statDiv = document.getElementById("container-stats")
    if (statDiv.hasChildNodes()) {
        oldDiv = statDiv.childNodes[0]
        oldDiv.remove()
    }
    newDiv = document.createElement("DIV")
    newDiv.id = "statsGrid"
    statDiv.append(newDiv)

    const grid = new Grid({
        columns: df_final.columns,
        data: df_final.data,
        pagination: {
          enabled: false,
          limit: 10,
          summary: false
        },
        search: true,
        sort: true
    }).render(newDiv);

    // Prepare data to CSV format
    const dataCSV = await df_final.to_csv(path.join(os.tmpdir(),"graphCorr_stats"))
    // Read data and put it in a xlsx workbook
    const statsWorkbook = XLSX.read(dataCSV, {type: 'string'})
    // Save the workbook to the app main folder in csv and xlsx formats
    XLSX.writeFile(statsWorkbook, path.join(os.tmpdir(), "graphCorr_stats.xlsx"))
    XLSX.writeFile(statsWorkbook, path.join(os.tmpdir(), "graphCorr_stats_csv"), {bookType: 'csv'})

    // Bind data files to the corresponding download buttons
    const saveXlsxButton = document.getElementById("statsXlsxButton")
    saveXlsxButton.href = path.join(os.tmpdir(), "graphCorr_stats.xlsx")
    const saveCsvButton = document.getElementById("statsCsvButton")
    saveCsvButton.href = path.join(os.tmpdir(), "graphCorr_stats.csv")

}


// Function called when the data page is loaded
async function generate_grid(pageLimit = GRIDPAGELIMIT) {
    GRIDPAGELIMIT = pageLimit
    createGrid(df_noNaN, pageLimit)
}


// Program the buttons for page limit selection
const pageLimitButtons = document.querySelectorAll(".page-lim-btn")
pageLimitButtons.forEach((btn, btnIndex, btnArray) => {
    const lims = [10, 20, 50, 100, 200, 500, 1000, "inf"]
    lims.forEach((value) => {
        if (btn.classList.contains(`page-lim-btn-${value}`)) {
            btn.addEventListener('click', () => {
                generate_grid(value)
                btnArray.forEach((clickedBtn) => {
                    clickedBtn.classList.remove("active")
                })
                btn.classList.add("active")
            })
        }
    })
})


// Prepare data and bind it to an interactive grid
function createGrid(df, pageLimit = GRIDPAGELIMIT) {
    // Disable pagination if number of rows is inferior to number of data entries
    // or if pageLimit is a string ("inf")
    activatePagination = pageLimit < df.data.length

    // Prepare a fresh div to contain the data grid
    statDiv = document.getElementById("container-data")
    if (statDiv.hasChildNodes()) {
        oldDiv = statDiv.childNodes[0]
        oldDiv.remove()
    }
    newDiv = document.createElement("DIV")
    newDiv.id = "dataGrid"
    statDiv.append(newDiv)

    const grid = new Grid({
        columns: df.columns.map(setup_columns),
        data: df.data,
        pagination: {
          enabled: activatePagination,
          limit: pageLimit,
          summary: false
        },
        fixedHeader: true,
        height: 600,
        search: true,
        sort: true
    }).render(newDiv);
}


// Define comparison operator for mixed arrays containing numbers, strings representing numbers, and strings
// Number and strings representing numbers should be sorted according to number value
// Strings that do not represent numbers should arrive in last position, sorted alphabetically
const sort_col = (a, b) => {
    if ((typeof parseFloat(a) === "number") && (typeof parseFloat(b) === "number") && (!isNaN(parseFloat(a))) && (!isNaN(parseFloat(b)))) {
        if (parseFloat(a) > parseFloat(b)) {
            return 1;
        }
        else if (parseFloat(b) > parseFloat(a)) {
            return -1;
        } else {
            return 0;
        }
    } else if (((typeof a === "string") && (typeof b === "string")) || ((typeof a === "number") && (typeof b === "number"))) {
        if (a > b) {
            return 1;
        } else if (b < a) {
            return -1;
        } else {
            return 0;
        }
    } else {
        if ((typeof parseFloat(a) === "number") && (!isNaN(parseFloat(a))) && ((typeof b === "string") || (isNaN(parseFloat(b))))) {
            return 1
        } else if ((typeof parseFloat(b) === "number") && (!isNaN(parseFloat(b))) && ((typeof a === "string") || (isNaN(parseFloat(a))))) {
            return -1
        } else {
            return 0
        }
    }
}

// Function mapping the name of a column to an object with the right "sort" properties
function setup_columns(col) {
    if (col.match(/\/\d/)) {
        return {
            name: col,
            sort: {
                compare: sort_col
            }
        }
    } else {
        return {
            name: col
        }
    }
}


// Open all links externally
const links = document.querySelectorAll('a[href]')

Array.prototype.forEach.call(links, (link) => {
  const url = link.getAttribute('href')
  if (url.indexOf('http') === 0) {
    link.addEventListener('click', (e) => {
      e.preventDefault()
      shell.openExternal(url)
    })
  }
})

// Data example on welcome-screen
const grid = new Grid({
    columns: ["Matricule","Année juin /20","Année août /20"].map(setup_columns),
    data: [
    [26896,"A",10],
    [36584,10.5,10.5],
    [29945,15,"NDP"],
    [43891,15.5,15.5]],
    pagination: {
      enabled: false,
      limit: 10,
      summary: false
    },
    search: true,
    sort: true
}).render(document.getElementById("dataExample"));


//
// NAVIGATION
//

// Nav buttons events for switching from views to views
const l_nav_btn = ["data", "graphs", "stats"]

l_nav_btn.forEach((btn_name) => {
    let nav_btn = document.getElementById("btn-" + btn_name)

    nav_btn.addEventListener("click", () => {
        l_nav_btn.forEach((l_name) => {
            if (btn_name == l_name) {
                document.getElementById("wrap-" + l_name).style.display = "block"
                document.getElementById("btn-" + l_name).classList.add("activeSection");
            } else {
                document.getElementById("wrap-" + l_name).style.display = "none"
                document.getElementById("btn-" + l_name).classList.remove("activeSection");
            }
        })

        if (btn_name == "data") {
            generate_grid()
        } else if (btn_name =="graphs") {
            document.getElementById("graphs-summary-tab").click()
        } else if (btn_name == "stats") {
            generate_stats()
        }

    })
})


////// Window minimize, maximize and close buttons //////
function init() {
    document.getElementById("min-btn").addEventListener("click", function (e) {
         var window = remote.getCurrentWindow();
         window.minimize();
    });

    document.getElementById("max-btn").addEventListener("click", function (e) {
         var window = remote.getCurrentWindow();
         if (!window.isMaximized()) {
             window.maximize();
         } else {
             window.unmaximize();
         }
    });

    document.getElementById("close-btn").addEventListener("click", function (e) {
         var window = remote.getCurrentWindow();
         window.close();
    });
};

document.onreadystatechange = function () {
    if (document.readyState == "complete") {
         init();
    }
};
