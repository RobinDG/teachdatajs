const { app, BrowserWindow } = require('electron')

if ( require( "electron-squirrel-startup" ) ) app.exit()

function createWindow () {
  const win = new BrowserWindow({
    width: 1300,
    height: 900,
    minWidth: 1000,
    minHeight: 790,
    frame: false,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      // devTools: false
    }
  })

  win.loadFile('index.html')
  // win.webContents.openDevTools()

  // If a new window is opened (ex: from "edit chart" plotly link) deactivate the "frameless" option for it
  win.webContents.on('new-window', (event, url, frameName, disposition, options, additionalFeatures, referrer, postBody) => {
      event.preventDefault()
      const newWin = new BrowserWindow({
          webContents: options.webContents, // use existing webContents if provided
          show: false,
          width: 1300,
          height: 1000,
          frame: true
      })
      newWin.once('ready-to-show', () => newWin.show())
      if (!options.webContents) {
          const loadOptions = {
              httpReferrer: referrer
          }
          if (postBody != null) {
              const { data, contentType, boundary } = postBody
              loadOptions.postData = postBody.data
              loadOptions.extraHeaders = `content-type: ${contentType}; boundary=${boundary}`
          }

          newWin.loadURL(url, loadOptions) // existing webContents will be navigated automatically
      }
      event.newGuest = newWin
  })
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})


// app.on('activate', () => {
//   if ((BrowserWindow.getAllWindows().length === 0) && (app.isReady())) {
//     createWindow()
//   }
// })
